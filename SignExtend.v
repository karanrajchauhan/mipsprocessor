`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:28:59 11/13/2017 
// Design Name: 
// Module Name:    SignExtend 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module SignExtend(
	input [15:0] extendee,			// value to be extended
	output [31:0] extended		// extended value
    );
	
	// first 31:16 same as 15th bit, 15:0 stay the same
	assign extended = {{16{extendee[15]}}, extendee[15:0]};

endmodule
