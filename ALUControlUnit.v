`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:50:57 11/13/2017 
// Design Name: 
// Module Name:    ALUControlUnit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ALUControlUnit (
	input clk,    						// clock
	input [1:0] alu_op,				// alu op received from control unit
	input [5:0] instr_funct,		// funct field of instruction
	
	output reg [3:0] alu_control
);

	always @ (*)
	begin

		case (alu_op)

			// addi or lw or sw (add)
			2'b00:
			begin
				alu_control = 4'b0010;
			end

			// beq (sub)
			2'b01:
			begin
				alu_control = 4'b0110;
			end

			// r-type
			2'b10:
			begin
				case (instr_funct)

					// add
					6'b100000:
					begin
						alu_control = 4'b0010;
					end

					// sub
					6'b100010:
					begin
						alu_control = 4'b0110;
					end

					// and
					6'b100100:
					begin
						alu_control = 4'b0000;
					end

					// or
					6'b100101:
					begin
						alu_control = 4'b0001;
					end
					
					// nor
					6'b100111:
					begin
						alu_control = 4'b1100;
					end

					// slt
					6'b101010:
					begin
						alu_control = 4'b0111;
					end
					
					// should never reach here, but if it does, treat as sub
					default:
					begin
						alu_control = 4'b0110;
					end

				endcase
			end

			// should never reach here, but if it does, treat as beq
			default:
			begin
				alu_control = 4'b0110;
			end

		endcase

	end

endmodule
