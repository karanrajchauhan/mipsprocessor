`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   13:36:53 11/13/2017
// Design Name:   SignExtend
// Module Name:   /ad/eng/users/c/h/chauhank/Documents/ec413/PROJECT/tb_SignExtend.v
// Project Name:  PROJECT
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: SignExtend
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module tb_SignExtend;
	
	reg clk;
	
	// Inputs
	reg [15:0] extendee;

	// Outputs
	wire [31:0] extended;

	// Instantiate the Unit Under Test (UUT)
	SignExtend uut (
		.extendee(extendee), 
		.extended(extended)
	);
	
	// clk generator
	always #1 clk = ~clk;

	initial begin
		// Initialize Inputs
		clk = 0;
		extendee = 0;

		// Wait 10 ns for global reset to finish
		#10;
		$display ("======================== START ========================");
		
		extendee  <= 16'b0011100110100010;
		repeat (1) @ (posedge clk);

		extendee  <= 16'b1110001010100001;
		repeat (1) @ (posedge clk);

	end
	
	always @ (posedge clk) begin 
		$display ("extendee [%h] extended [%h]", extendee, extended); 
	end
      
endmodule
