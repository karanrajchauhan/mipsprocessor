`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   18:31:46 11/13/2017
// Design Name:   ControlUnit
// Module Name:   /ad/eng/users/c/h/chauhank/Documents/ec413/PROJECT/tb_ControlUnit.v
// Project Name:  PROJECT
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ControlUnit
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module tb_ControlUnit;

	// Inputs
	reg clk;
	reg [31:0] instruction;

	// Outputs
	wire reg_dst;
	wire reg_write;
	wire alu_src;
	wire branch;
	wire mem_read;
	wire mem_write;
	wire mem_to_reg;
	wire [1:0] alu_op;

	// Instantiate the Unit Under Test (UUT)
	ControlUnit uut (
		.clk(clk), 
		.instruction(instruction), 
		.reg_dst(reg_dst), 
		.reg_write(reg_write), 
		.alu_src(alu_src), 
		.branch(branch), 
		.mem_read(mem_read), 
		.mem_write(mem_write), 
		.mem_to_reg(mem_to_reg), 
		.alu_op(alu_op)
	);
	
	// clk generator
	always #1 clk = ~clk;

	initial begin
		// Initialize Inputs
		clk = 0;
		instruction = 0;
		
		// Wait 10 ns for global reset to finish
		#10;
		$display ("======================== START ========================");
		repeat (1) @ (posedge clk);
		
		// r type - and
		instruction <= 32'b00000000011000010001000000000000;
		repeat (1) @ (posedge clk);
		
		// r type - or
		instruction <= 32'b00000000100000010001100000000001;
		repeat (1) @ (posedge clk);
		
		// r type - add
		instruction <= 32'b00000000101000010010000000000001;
		repeat (1) @ (posedge clk);
		
		// lw
		instruction <= 32'b10001100001001000000000110000000;
		repeat (1) @ (posedge clk);
		
		// sw
		instruction <= 32'b10101100001000100000001000000000;
		repeat (1) @ (posedge clk);
		
		// beq
		instruction <= 32'b00010000000001000000000000000100;
		repeat (1) @ (posedge clk);

	end
	
	always @ (posedge clk) begin 
		$display ("\n");
		$display ("RegDst [%b], ALUSrc [%d] MemtoReg [%b] RegWrite [%b] MemRead [%b] MemWrite [%b] Branch [%b] ALUOp [%b]", 
						reg_dst, alu_src, mem_to_reg, reg_write, mem_read, mem_write, branch, alu_op); 
	end
      
endmodule

