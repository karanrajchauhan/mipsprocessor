`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:59:38 11/13/2017 
// Design Name: 
// Module Name:    DataMemory 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module DataMemory(
	input clk,
	input rst,
	input mem_write,					// Write to memory flag
	input mem_read,						// Read from memory flag
	input [7:0] address,				// 8-bit address
	input [31:0] write_data,			// Data to write to memory
	
	output [31:0] read_data				// Data read from memory
);

	reg [31:0] memory[0:255];			// 255 (2^8 - 1) registers, each is 32-bit

	// combinatorial reading of data memory at specified address
	assign read_data = (mem_read == 1'b1) ? memory[address] : read_data;

	integer i;
	always @ (posedge clk)
	begin
	
		// reset to 0's if rst is asserted
		if (rst) 
		begin
			for (i=0; i<256; i=i+1) memory[i] <= 32'b0;
		end
		
		// if asserted, write the writedata to specified address
		else if (mem_write)
		begin
			memory[address] <= write_data;		
		end
	
	end

endmodule
