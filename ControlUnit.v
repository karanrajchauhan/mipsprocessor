`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:17:59 11/13/2017 
// Design Name: 
// Module Name:    ControlUnit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ControlUnit (
	input clk,    // Clock
	input [31:0] instruction,

	output reg reg_dst,
	output reg reg_write,
	output reg alu_src,
	output reg branch,
	output reg mem_read,
	output reg mem_write,
	output reg mem_to_reg,
	output reg [1:0] alu_op
);

	// only the opcode dictates output of control unit
	wire [5:0] instr_op = instruction[31:26];

	always @ (*)
	begin
	
		case (instr_op)

			// r-type instruction
			6'b000000:
			begin
				reg_dst 		<= 1'b1;
				alu_src 		<= 1'b0;
				mem_to_reg 	<= 1'b0;
				reg_write 	<= 1'b1;
				mem_read 	<= 1'b0;
				mem_write 	<= 1'b0;
				branch 		<= 1'b0;
				alu_op 		<= 2'b10;
			end
			
			// addi
			6'b001000:
			begin
				reg_dst 		<= 1'b0;
				alu_src 		<= 1'b1;
				mem_to_reg 	<= 1'b0;
				reg_write 	<= 1'b1;
				mem_read 	<= 1'b0;
				mem_write 	<= 1'b0;
				branch 		<= 1'b0;
				alu_op 		<= 2'b0;
			end

			// lw
			6'b100011:
			begin
				reg_dst 		<= 1'b0;
				alu_src 		<= 1'b1;
				mem_to_reg 	<= 1'b1;
				reg_write 	<= 1'b1;
				mem_read 	<= 1'b1;
				mem_write 	<= 1'b0;
				branch 		<= 1'b0;
				alu_op 		<= 2'b00;
			end

			// sw
			6'b101011:
			begin
				reg_dst 		<= 1'b0;		// dont care
				alu_src 		<= 1'b1;
				mem_to_reg 	<= 1'b0;		// dont care
				reg_write 	<= 1'b0;
				mem_read 	<= 1'b0;
				mem_write 	<= 1'b1;		// dont care
				branch 		<= 1'b0;
				alu_op 		<= 2'b00;
			end

			// beq
			6'b000100:
			begin
				reg_dst 		<= 1'b0;		// dont care
				alu_src 		<= 1'b0;
				mem_to_reg 	<= 1'b0;		// dont care
				reg_write 	<= 1'b0;
				mem_read 	<= 1'b0;
				mem_write 	<= 1'b0;
				branch 		<= 1'b1;
				alu_op 		<= 2'b01;
			end

			// should never reach here, but if it does, treat as r-type
			default:
			begin
				reg_dst 		<= 1'b1;
				alu_src 		<= 1'b0;
				mem_to_reg 	<= 1'b0;
				reg_write 	<= 1'b1;
				mem_read 	<= 1'b0;
				mem_write 	<= 1'b0;
				branch 		<= 1'b0;
				alu_op 		<= 2'b10;
			end

		endcase

	end

endmodule
