`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   13:01:37 11/13/2017
// Design Name:   DataMemory
// Module Name:   /ad/eng/users/c/h/chauhank/Documents/ec413/PROJECT/tb_DataMemory.v
// Project Name:  PROJECT
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: DataMemory
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module tb_DataMemory;

	// Inputs
	reg clk;
	reg rst;
	reg mem_write;
	reg mem_read;
	reg [7:0] address;
	reg [31:0] write_data;

	// Outputs
	wire [31:0] read_data;

	// Instantiate the Unit Under Test (UUT)
	DataMemory uut (
		.clk(clk),
		.rst(rst),
		.mem_write(mem_write), 
		.mem_read(mem_read), 
		.address(address), 
		.write_data(write_data), 
		.read_data(read_data)
	);
	
	// clk generator
	always #1 clk = ~clk;

	initial begin
		clk = 0;
		rst = 1;
		mem_write = 0;
		mem_read  = 0;
		
		address = 7'b0;
		write_data = 32'b0;
		
		#100 rst = 0;
		
		$display ("======================== START ========================");
		repeat (1) @ (posedge clk);

		write_data  <= 32'h0000_0000_0000_0002;
		address    <= 8'b00000100;  
		mem_write   <= 1'b1;
		repeat (1) @ (posedge clk);

		write_data <= 32'h0000_0000_0000_0005;
		address      <= 8'b00001000;  
		mem_write     <= 1'b1;
		repeat (1) @ (posedge clk);

		write_data <= 32'h0000_0000_0000_0009;
		address      <= 8'b00001100;  
		mem_write     <= 1'b1;
		repeat (1) @ (posedge clk);

		write_data <= 32'h0000_0000_0000_0007;
		address      <= 8'b00011000;  
		mem_write     <= 1'b1;
		repeat (1) @ (posedge clk);

		write_data <= 32'h0000_0000_0000_000A;
		address      <= 8'b00011100;  
		mem_write     <= 1'b1;
		repeat (1) @ (posedge clk);

		address      <= 8'b00011000;  
		mem_write     <= 1'b0;
		mem_read      <= 1'b1;
		repeat (1) @ (posedge clk);

		address      <= 8'b00000100;  
		mem_read      <= 1'b1;
		repeat (1) @ (posedge clk);

		address      <= 8'b00011100;  
		mem_read      <= 1'b1;
		repeat (1) @ (posedge clk);

		address      <= 8'b00001000;  
		mem_read      <= 1'b1;
		repeat (1) @ (posedge clk);

		address      <= 8'b00001100;  
		mem_read      <= 1'b1;
		repeat (1) @ (posedge clk);          
	end


	always @ (posedge clk) begin
		$display ("\n");
		$display ("mem_write [%b], address [%h] write_data [%h]", mem_write, address, write_data); 
		$display ("mem_read  [%b], address [%h] read_data  [%h]", mem_read,  address, read_data); 
	end
      
endmodule
