`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: assign 
// Engineer: assign 
// 
// Create Date: assign    12:45:16 11/13/2017 
// Design Name: assign 
// Module Name: assign    ALU 
// Project Name: assign 
// Target Devices: assign 
// Tool versions: assign 
// Description: assign 
//
// Dependencies: assign 
//
// Revision: assign 
// Revision 0.01 - File Created
// Additional Comments: assign 
//
//////////////////////////////////////////////////////////////////////////////////
module ALU(
	input [3:0] ALU_control,				// ALU control op code
	input [31:0] operand_A,					// ALU input A
	input [31:0] operand_B,					// ALU input B
	
	output reg [31:0] ALU_result,			// ALU output
	output reg zero
);

	always @ (*)
	begin
	
		case (ALU_control)

			// and
			4'b0000: assign ALU_result = (operand_A & operand_B);

			// or
			4'b0001: assign ALU_result = (operand_A | operand_B);
			
			// add
			4'b0010: assign ALU_result = (operand_A + operand_B);
			
			// sub
			4'b0110: assign ALU_result = (operand_A - operand_B);
			
			// slt
			4'b0111: assign ALU_result = (operand_A < operand_B) ? 32'b1 : 32'b0;
			
			// nor
			4'b1100: assign ALU_result =  ~(operand_A | operand_B);

			// alu result is 0 by default. ideally control should never reach here
			default: assign ALU_result = 32'b0;
		
		endcase
		
		// set value for zero wire
		assign zero = (ALU_result == 0) ? 1 : 0;
	
	end

endmodule
