`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   12:38:40 11/13/2017
// Design Name:   RegisterFile
// Module Name:   /ad/eng/users/c/h/chauhank/Documents/ec413/PROJECT/tb_RegisterFile.v
// Project Name:  PROJECT
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: RegisterFile
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module tb_RegisterFile;

	// Inputs
	reg clk;
	reg rst;
	reg [4:0] read1_sel;
	reg [4:0] read2_sel;
	reg [4:0] write_sel;
	reg [31:0] write_data;
	reg write_en;

	// Outputs
	wire [31:0] read1_data;
	wire [31:0] read2_data;

	// Instantiate the Unit Under Test (UUT)
	RegisterFile uut (
		.clk(clk), 
		.rst(rst), 
		.read1_sel(read1_sel), 
		.read2_sel(read2_sel), 
		.write_sel(write_sel), 
		.write_data(write_data), 
		.write_en(write_en), 
		.read1_data(read1_data), 
		.read2_data(read2_data)
	);
	
	// clk generator
	always #1 clk = ~clk;
	
	initial begin
		clk = 0;
		rst = 1;
		
		write_en = 0;
		write_sel = 5'b0;
		write_data = 32'b0;
		
		read1_sel = 5'b0;
		read2_sel = 5'b0;
		
		#10 rst = 0; 
		$display ("======================== START ========================");
		repeat (1) @ (posedge clk);

		write_data <= 32'h0000_0000_0000_0002;
		write_sel  <= 5'b00001;  
		write_en      <= 1'b0;
		repeat (1) @ (posedge clk);

		write_data <= 32'h0000_0000_0000_0002;
		write_sel  <= 5'b00001;  
		write_en      <= 1'b1;
		repeat (1) @ (posedge clk);

		write_data <= 32'h0000_0000_0000_0005;
		write_sel  <= 5'b00011;  
		write_en      <= 1'b1;
		repeat (1) @ (posedge clk);

		write_data <= 32'h0000_0000_0000_0009;
		write_sel  <= 5'b00111;  
		write_en      <= 1'b1;
		repeat (1) @ (posedge clk);

		write_data <= 32'h0000_0000_0000_0007;
		write_sel  <= 5'b00000;  
		write_en      <= 1'b1;
		repeat (1) @ (posedge clk);

		write_data <= 32'h0000_0000_0000_000A;
		write_sel  <= 5'b00101;  
		write_en      <= 1'b1;
		read1_sel  <= 5'b00101; 
		repeat (1) @ (posedge clk);

		write_data <= 32'h0000_0000_0000_000A;
		write_sel  <= 5'b00101;  
		write_en      <= 1'b0;
		read1_sel  <= 5'b00101; 
		repeat (1) @ (posedge clk);

		write_data <= 32'h0000_0000_0000_000A;
		write_sel  <= 5'b00101;  
		write_en      <= 1'b0;
		read1_sel  <= 5'b00101; 
		read2_sel  <= 5'b00101; 
		repeat (1) @ (posedge clk);

		read1_sel  <= 5'b00111; 
		read2_sel  <= 5'b00111; 
		repeat (1) @ (posedge clk);

		read1_sel  <= 5'b00011; 
		read2_sel  <= 5'b00001; 
		repeat (1) @ (posedge clk);

		read1_sel  <= 5'b00000; 
		read2_sel  <= 5'b00001; 
		repeat (1) @ (posedge clk);
		end


		always @ (posedge clk) begin 
			$display ("\n");
			$display ("Write En [%d], Write Sel [%d], Write Data [%h]", write_en, write_sel, write_data); 
			$display ("Read1 Sel [%d], Read1 Data [%h]", read1_sel, read1_data); 
			$display ("Read2 Sel [%d], Read2 Data [%h]", read2_sel, read2_data); 
		end
      
endmodule
