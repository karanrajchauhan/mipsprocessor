`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   12:46:04 11/13/2017
// Design Name:   ALU
// Module Name:   /ad/eng/users/c/h/chauhank/Documents/ec413/PROJECT/tb_ALU.v
// Project Name:  PROJECT
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ALU
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module tb_ALU;

	reg clk;

	// Inputs
	reg [3:0] ALU_control;
	reg [31:0] operand_A;
	reg [31:0] operand_B;

	// Outputs
	wire [31:0] ALU_result;
	wire zero;

	// Instantiate the Unit Under Test (UUT)
	ALU uut (
		.ALU_control(ALU_control), 
		.operand_A(operand_A), 
		.operand_B(operand_B), 
		.ALU_result(ALU_result), 
		.zero(zero)
	);

	// clk generator
	always #1 clk = ~clk;
	
	initial begin
		clk = 0;
		ALU_control = 4'b0;
		operand_A = 32'b0;
		operand_B = 32'b0;
		
		$display ("======================== START ========================");
		repeat (1) @ (posedge clk);

		operand_A   <= 32'h0000_0000_0000_0007;
		operand_B   <= 32'h0000_0000_0000_000B;
		ALU_control <= 4'b0000;  
		repeat (1) @ (posedge clk);

		operand_A   <= 32'h0000_0000_0000_0002;
		operand_B   <= 32'h0000_0000_0000_0004;
		ALU_control <= 4'b0001;  
		repeat (1) @ (posedge clk);

		operand_A   <= 32'h0000_0000_0000_000A;
		operand_B   <= 32'h0000_0000_0000_0004;
		ALU_control <= 4'b0110;  
		repeat (1) @ (posedge clk);

		operand_A   <= 32'h0000_0000_0000_0002;
		operand_B   <= 32'h0000_0000_0000_0004;
		ALU_control <= 4'b0111;  
		repeat (1) @ (posedge clk);

		operand_A   <= 32'h0000_0000_0000_0002;
		operand_B   <= 32'h0000_0000_0000_0004;
		ALU_control <= 4'b1100;  
		repeat (1) @ (posedge clk);
		
		operand_A   <= 32'h0000_0000_0000_0001;
		operand_B   <= 32'h0000_0000_0000_0000;
		ALU_control <= 4'b0000;  
		repeat (1) @ (posedge clk);
		
	end


	always @ (posedge clk) begin 
		$display ("\n");
		$display ("ALU_control [%b], operand_A [%d] operand_B [%d]", ALU_control, operand_A, operand_B); 
		$display ("ALU_result [%d] zero  [%b]", ALU_result, zero); 
	end
      
endmodule

