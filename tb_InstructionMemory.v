`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   19:36:16 12/05/2017
// Design Name:   InstructionMemory
// Module Name:   /ad/eng/users/c/h/chauhank/Documents/ec413/PROJECT/tb_InstructionMemory.v
// Project Name:  PROJECT
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: InstructionMemory
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module tb_InstructionMemory;

	// Inputs
	reg clk;
	reg rst;
	reg [7:0] address;

	// Outputs
	wire [31:0] instruction;

	// Instantiate the Unit Under Test (UUT)
	InstructionMemory uut (
		.clk(clk), 
		.rst(rst), 
		.address(address), 
		.instruction(instruction)
	);

	// Clock generator
	always #1 clk = ~clk;
	
	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 0;
		address = 0;
		
		// wait to finish initialization
		#100;
		
//		$display ("======================== START ========================");
//		repeat (10) @ (posedge clk)
		
		uut.memory[8'b0]		= 32'b000000_00001_00010_00011_00000_100000;
		uut.memory[8'b1] 	= 32'b000100_00011_00100_0000000000000011;
		uut.memory[8'b10]	= 32'b000000_00100_00010_00011_00000_100000;
		uut.memory[8'b11]	= 32'b000000_00100_00001_00011_00000_100000;
		uut.memory[8'b100]	= 32'b000000_00011_00011_00011_00000_100000;
		
		#10;
		uut.memory[8'b0]		= 32'b000000_00001_00010_00011_00000_100010;
		uut.memory[8'b1] 	= 32'b000100_00011_00100_0000000000001011;
		uut.memory[8'b10]	= 32'b000000_00100_00010_00011_00000_101000;
		uut.memory[8'b11]	= 32'b000000_00100_00001_00011_00000_101000;
		uut.memory[8'b100]	= 32'b000000_00011_00011_00011_00000_110000;

	end
	
	always @ (posedge clk) begin
		$display ("\n");
		$display ("mem0=[%b]", uut.memory[8'b0]);
		$display ("mem1=[%b]", uut.memory[8'b1]);
		$display ("mem2=[%b]", uut.memory[8'b10]);
		$display ("mem3=[%b]", uut.memory[8'b11]);
		$display ("mem4=[%b]", uut.memory[8'b1]);
	end
      
endmodule

