`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:21:57 12/05/2017 
// Design Name: 
// Module Name:    InstructionMemory 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module InstructionMemory (
	input clk,						// clock
	input rst,						// reset to nops
	input [7:0] address,			// address to be read

	output [31:0] instruction 	// instruction at address
);

	// memory where instructions are stored
	reg[31:0] memory[0:255];

	// combinatorial reading of instruction memory
	assign instruction = memory[address];

	// write 0's to instruction memory when rst is asserted on posedge
	integer i;
	always @ (posedge clk)
	begin
		// reset to 0's if rst is asserted
		if (rst)
		begin
			for (i=0; i<256; i=i+1) memory[i] = 32'b0;
		end
	end


endmodule
