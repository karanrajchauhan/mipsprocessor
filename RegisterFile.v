`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:38:00 11/13/2017 
// Design Name: 
// Module Name:    RegisterFile 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module RegisterFile(
	input clk,
	input rst,

	input [4:0] read1_sel,				// Read register 1
	input [4:0] read2_sel,				// Read register 2
	input [4:0] write_sel,				// Write register
	input [31:0] write_data,			// Write data
	input write_en,						// RegWrite
	
	output [31:0] read1_data,		// Read data 1
	output [31:0] read2_data		// Read data 2
);

	// TODO: should make blocking assignments?

	reg [31:0] registers[0:31];		// 32 registers, each is 32-bit
	
	
	// combinatorial - load data in read register i into read data i
	assign read1_data = registers[read1_sel];
	assign read2_data = registers[read2_sel];
	
	integer i;
	always @ (posedge clk)
	begin
	
		// reset to 0's if rst is asserted
		if (rst) 
		begin
			for (i=0; i<32; i=i+1) registers[i] <= 32'b0;
		end
		
		// if RegWrite is asserted then write data to write register
		else if (write_en)
		begin
			// MUST. NOT. OVERWRITE. r0 !!!
			if (write_sel != 5'b0)
			begin
				registers[write_sel] <= write_data;
			end
		end

	end

endmodule
