`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   20:29:11 11/13/2017
// Design Name:   CPU
// Module Name:   /ad/eng/users/c/h/chauhank/Documents/ec413/PROJECT/tb_CPU.v
// Project Name:  PROJECT
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: CPU
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module tb_CPU;

	// Inputs
	reg clk;
	reg rst;
	reg start;

	// Instantiate the Unit Under Test (UUT)
	CPU uut (
		.clk(clk), 
		.rst(rst), 
		.start(start)
	);

	// Clock generator
	always #1 clk = ~clk;

	initial begin

		// Initialize Inputs
		clk = 0;
		rst = 1;
		start = 1;

		// everything has been set, start writing
		#10 rst = 0;
		
///////////////////////////// AND, OR, ADD, SUB, SLT, NOR, ADDI TESTING /////////////////////////////

		// write 2 to reg1, 4 to reg2, 6 to reg3
		// 2, 4, 6
		uut.reg_file.registers[8'b1] 		= 32'b10;
		uut.reg_file.registers[8'b10] 	= 32'b100;
		uut.reg_file.registers[8'b11] 	= 32'b110;
		
		// add reg1, reg2, reg3
		// 10, 4, 6
		uut.instruction_memory.memory[8'b0]		= 32'b000000_00010_00011_00001_00000_100000;

		// or reg3, reg1, reg2
		// 10, 4, 14
		uut.instruction_memory.memory[8'b1]		= 32'b000000_00001_00010_00011_00000_100101;
		
		// and reg2, reg1, reg3
		// 10, 10, 14
		uut.instruction_memory.memory[8'b10]	= 32'b000000_00001_00011_00010_00000_100100;
		
		// sub reg2, reg3, reg1
		// 10, 4, 14
		uut.instruction_memory.memory[8'b11]	= 32'b000000_00011_00001_00010_00000_100010;
		
		// slt reg3, reg2, reg1
		// 10, 4, 1
		uut.instruction_memory.memory[8'b100]	= 32'b000000_00010_00001_00011_00000_101010;
		
		// nor reg1, reg2, reg3
		// some_big_value, 4, 1
		uut.instruction_memory.memory[8'b101]	= 32'b000000_00010_00011_00001_00000_100111;
		
		// addi reg1, reg2, 3
		// 7, 4, 1
		uut.instruction_memory.memory[8'b110]	= 32'b001000_00010_00001_0000000000000011;

/////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////// LW, SW TESTING //////////////////////////////////////////
//
//		// write 5 to reg1, 7 to reg2, 12 to mem[0], 13 to mem[1]
//		uut.reg_file.registers[8'b1] 		= 32'b101;
//		uut.reg_file.registers[8'b10] 	= 32'b111;
//		uut.data_Memory.memory[8'b0] 		= 32'b1100;
//		uut.data_Memory.memory[8'b1] 		= 32'b1101;
//
//		// load content at mem[0] into reg1
//		// lw reg1, 0(reg0)
//		uut.instruction_memory.memory[8'b0]		= 32'b100011_00000_00001_0000000000000000;
//	
//		// store content of reg2 into mem[1]
//		// sw reg2, 1(reg0)
//		uut.instruction_memory.memory[8'b1] 	= 32'b101011_00000_00010_0000000000000001;
//
/////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////// BEQ TESTING ////////////////////////////////////////////
//
//		// write 5 to reg1, 7 to reg2, 12 to reg4
//		// 5,7,0,12
//		uut.reg_file.registers[8'b1] 		= 32'b101;
//		uut.reg_file.registers[8'b10] 	= 32'b111;
//		uut.reg_file.registers[8'b100] 	= 32'b1100;
//		
//		// ADD reg1 and reg2, put result in reg3
//		// 5,7,12,12
//		uut.instruction_memory.memory[8'b0]		= 32'b000000_00001_00010_00011_00000_100000;
//	
//		// BEQ reg3, reg4, offset=1
//		uut.instruction_memory.memory[8'b1] 	= 32'b000100_00011_00100_0000000000000001;
//
//		// ADD reg4 and reg2, put result in reg3
//		// 5,7,19,12
//		uut.instruction_memory.memory[8'b10]	= 32'b000000_00100_00010_00011_00000_100000;
//
//		// ADD reg1 and reg4, put result in reg3 
//		// 5,7,17,12
//		uut.instruction_memory.memory[8'b11]	= 32'b000000_00100_00001_00011_00000_100000;
//		
//		// ADD reg3 and reg3, put result in reg3 
//		// 5,7,34,12
//		uut.instruction_memory.memory[8'b100]	= 32'b000000_00011_00011_00011_00000_100000;
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

		$display ("======================== START ========================");
		repeat (1) @ (posedge clk);		
	
	end
	
	// fixme: value isn't updated in one cycle
	always @ (posedge clk) begin
		$display ("\n");
		$display ("program counter=[%d], instr=[%b]", uut.program_counter, uut.instruction);
		$display ("reg1=[%d], reg2=[%d], reg3=[%d]", uut.reg_file.registers[8'b1], uut.reg_file.registers[8'b10], uut.reg_file.registers[8'b11]); 

//		$display ("reg1=[%d], reg2=[%d], mem0=[%d], mem1=[%d]", uut.reg_file.registers[8'b1], uut.reg_file.registers[8'b10], uut.data_Memory.memory[8'b0], uut.data_Memory.memory[8'b1]); 

//		$display ("branch=[%d], alu0=[%b]", uut.branch, uut.ALU_zero);
//		$display ("reg1=[%d], reg2=[%d], reg3=[%d], reg4=[%d]", uut.reg_file.registers[8'b1], uut.reg_file.registers[8'b10], uut.reg_file.registers[8'b11], uut.reg_file.registers[8'b100]); 
	end
      
endmodule
