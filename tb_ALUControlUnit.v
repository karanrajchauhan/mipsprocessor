`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   19:55:02 11/13/2017
// Design Name:   ALUControlUnit
// Module Name:   /ad/eng/users/c/h/chauhank/Documents/ec413/PROJECT/tb_ALUControlUnit.v
// Project Name:  PROJECT
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ALUControlUnit
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module tb_ALUControlUnit;

	// Inputs
	reg clk;
	reg [1:0] alu_op;
	reg [5:0] instr_funct;

	// Outputs
	wire [3:0] alu_control;

	// Instantiate the Unit Under Test (UUT)
	ALUControlUnit uut (
		.clk(clk), 
		.alu_op(alu_op), 
		.instr_funct(instr_funct), 
		.alu_control(alu_control)
	);
	
	// clk generator
	always #1 clk = ~clk;

	initial begin
		// Initialize Inputs
		clk = 0;
		alu_op = 0;
		instr_funct = 0;

		// Wait 10 ns for global reset to finish
		#10;
		
		$display ("======================== START ========================");
		repeat (1) @ (posedge clk);
		
		// test lw, sw. should output 0010
		$display ("should output 0010");
		alu_op  		<= 2'b00;
		instr_funct <= 6'b000100;  
		repeat (1) @ (posedge clk);
		
		alu_op  		<= 2'b00;
		instr_funct <= 6'b000100;  
		repeat (1) @ (posedge clk);
		
		// test beq. should output 0110
		$display ("should output 0110");
		alu_op  		<= 2'b01;
		instr_funct <= 6'b000100;  
		repeat (1) @ (posedge clk);
		
		alu_op  		<= 2'b01;
		instr_funct <= 6'b010100;  
		repeat (1) @ (posedge clk);
		
		// test add. should output 0010
		$display ("should output 0010");
		alu_op  		<= 2'b10;
		instr_funct <= 6'b100000;  
		repeat (1) @ (posedge clk);
		
		// test sub. should output 0110
		$display ("should output 0110");

		alu_op  		<= 2'b10;
		instr_funct <= 6'b100010;  
		repeat (1) @ (posedge clk);
		
		// test and. should output 0000
		$display ("should output 0000");
		alu_op  		<= 2'b10;
		instr_funct <= 6'b100100;  
		repeat (1) @ (posedge clk);
		
		// test or. should output 0001
		$display ("should output 0001");
		alu_op  		<= 2'b10;
		instr_funct <= 6'b100101;  
		repeat (1) @ (posedge clk);
		
		// test slt. should output 0111
		$display ("should output 0111");
		alu_op  		<= 2'b10;
		instr_funct <= 6'b101010;  
		repeat (1) @ (posedge clk);

	end
	
	always @ (posedge clk) begin
		$display ("alu_op [%b], funct [%b] alu_control [%b]", alu_op, instr_funct, alu_control); 
		$display ("\n");
	end
      
endmodule

