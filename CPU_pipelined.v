// TODO: make pc 8bit?
// TODO: change tb_CPU for instr is not reg anymore
// PC and adder outside CPU module?
// TODO: remove program counter as input?


`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:16:45 11/23/2017 
// Design Name: 
// Module Name:    CPU_pipelined 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module CPU (
  clk, 
  rst, 
  start
); 

	// inputs
	input clk, rst, start; 


//////////////////////////////////////////////////////////////////////////////////
//							INSTRUCTION FETCH REGISTERS 						//
//////////////////////////////////////////////////////////////////////////////////

	// program counter
	reg [31:0] program_counter;

	// pc+4 register in the if/id stage
	reg [31:0] ifid_pc4;

	// the new instruction read
	reg [31:0] ifid_instruction1;

	// the instruction read in previous cycle
	reg [31:0] ifid_instruction2;
	
	// ControlUnit signals
	reg reg_dst, reg_write, alu_src, branch, mem_write, mem_read, mem_to_reg;
	reg [1:0] ALU_op;
	
	//	ALUControlUnit signals
	reg [3:0] ALU_control;
	reg [5:0] instr_funct;
	
	// SignExtend connections
	reg [15:0] i_15_0;
	reg [31:0] sign_extended;
	
	// RegisterFile connections
	reg [4:0] i_25_21, i_20_16, i_15_11, write_reg;
	reg [31:0] read1_data, read2_data, write_data;

	// ALU connections
	reg [31:0] ALU_operand_B, ALU_result;
	reg ALU_zero;
	
	// DataMemory connections
	reg [31:0] mem_read_data;



//////////////////////////////////////////////////////////////////////////////////
//								INSTRUCTION FETCH 								//
//////////////////////////////////////////////////////////////////////////////////

	InstructionMemory instruction_memory (
		.clk(clk),
		.address(program_counter[7:0]),				// TODO: remove [7:0] if not 32-bit
		.instruction(ifid_instruction1)
	);

	// TODO: initialize program counter here using always @ begin?
	// TODO: what if pc has undefined value?

	always @ (posedge clk)
	begin

		// add 4 to PC
		program_counter = program_counter + 32'b100;

		if(branch & ALU_zero)
		begin

			// add left shifted sign extended immediate to PC
			program_counter = program_counter + (sign_extended << 2);
		
		end

	end




	// split instruction into useful parts
	assign i_25_21 		= instruction[25:21];
	assign i_20_16 		= instruction[20:16];
	assign i_15_11 		= instruction[15:11];
	assign i_15_0 			= instruction[15:0];
	assign instr_funct 	= instruction[5:0];
	
	ControlUnit control (
		.clk			(clk			), 
		.instruction(instruction), 
		.reg_dst		(reg_dst		), 
		.reg_write	(reg_write	), 
		.alu_src		(alu_src		),
		.branch		(branch		),
		.mem_read	(mem_read	), 
		.mem_write	(mem_write	), 
		.mem_to_reg	(mem_to_reg	), 
		.alu_op		(ALU_op		)
	);
	
	ALUControlUnit ALU_control_unit (
		.clk(clk), 
		.alu_op(ALU_op), 
		.instr_funct(instr_funct), 
		.alu_control(ALU_control)
	);
	
	SignExtend sign_extender (
		.extendee(i_15_0			), 
		.extended(sign_extended	)
	);

	// MUX to select write register input. Use i[15:11] if RegDst is asserted, i[20:16] otherwise
	assign write_reg = ( reg_dst==1'b1 ? i_15_11 : i_20_16 );
	
	RegisterFile reg_file (
		.clk			(clk			), 
		.rst			(rst			), 
		.read1_sel	(i_25_21		), 
		.read2_sel	(i_20_16		), 
		.write_sel	(write_reg	), 
		.write_data	(write_data	), 
		.write_en	(reg_write	), 
		.read1_data	(read1_data	), 
		.read2_data	(read2_data	)
	);
	
	// MUX to select ALU operand B input. Use sign extended is ALUsrc is asserted, read data 2 otherwise
	assign ALU_operand_B = ( alu_src==1'b1 ? sign_extended : read2_data);
	
	ALU ALU_unit (
		.ALU_control(ALU_control), 
		.operand_A(read1_data), 
		.operand_B(ALU_operand_B), 
		.ALU_result(ALU_result), 
		.zero(ALU_zero)
	);

	DataMemory data_memory (
		.clk			(clk		  		),
		.rst			(rst				),
		.mem_write	(mem_write 		), 
		.mem_read	(mem_read  		), 
		.address		(ALU_result		), 
		.write_data	(read2_data		), 
		.read_data	(mem_read_data	)
	);
	
	// MUX to select value of write data. Use DMem ReadData if MemtoReg is asserted, ALU result otherwise
	assign write_data = (mem_to_reg ? mem_read_data : ALU_result[7:0]);

	// update program counter and read instruction at each rising edge
	always @(posedge clk)
	begin

		// add 4 to PC
		program_counter  <= program_counter + 32'b100;

		if(branch & ALU_zero)
		begin

			// add left shifted sign extended immediate to PC
			program_counter <= program_counter + (sign_extended << 2);
		
		end

	end
	
	
endmodule
